const canvas = document.getElementById("myCanvas");
canvas.width = document.body.clientWidth;
canvas.height = document.body.clientHeight;
const ctx = canvas.getContext("2d");

const car = new Image();
const progressCar = new Image();
const group = new Image();
const bg = new Image();
const info = new Image();
const firstMap = new Image();

car.src = "./assets/images/car.png";
progressCar.src = "./assets/images/car.png";
group.src = "./assets/images/group.png";
bg.src = "./assets/images/Frame1.png";
info.src = "./assets/images/info.png";
firstMap.src = "./assets/images/maps/firstMap.png";

const steps = ["instruction", "game", "end"];
let gameOver = false;
let currentStep = 0;

const maps = [firstMap, firstMap, firstMap];

let upPressed = false;
let downPressed = false;
let rightPressed = false;

let progress = 0;
let mapsWidth = canvas.width * maps.length;

const cameraPos = {
  x: 0,
  y: 0,
};

const textures = [
  { image: group, x: canvas.width + 40, y: canvas.height / 2 - 20, angle: 0 },
  { image: group, x: canvas.width, y: canvas.height / 2 + 20, angle: 0 },
  {
    image: car,
    x: 100,
    y: canvas.height / 2,
    angle: 0,
    isMainCar: true,
  },
];

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function nextStep() {
  if (currentStep <= steps.length) {
    currentStep += 1;
  }
}

function keyDownHandler(e) {
  if (e.key == "Up" || e.key == "ArrowUp") {
    upPressed = true;
  } else if (e.key == "Down" || e.key == "ArrowDown") {
    downPressed = true;
  } else if (e.key == "Right" || e.key == "ArrowRight") {
    rightPressed = true;
  } else if (e.key == "Enter") {
    if (steps[currentStep] === "instruction") {
      if (cameraPos.y - info.height > 0) nextStep();
    }
  }
}

function keyUpHandler(e) {
  if (e.key == "Up" || e.key == "ArrowUp") {
    upPressed = false;
  } else if (e.key == "Down" || e.key == "ArrowDown") {
    downPressed = false;
  } else if (e.key == "Right" || e.key == "ArrowRight") {
    rightPressed = false;
  }
}

function rotateImage(image, x, y, angle) {
  ctx.save();
  ctx.translate(x + image.width / 2, y + image.height / 2);
  ctx.rotate((angle * Math.PI) / 180);
  ctx.drawImage(image, -image.width / 2, -image.height / 2);
  ctx.restore();
}

function drawTexture(texture) {
  const { image, x, y, angle, isMainCar } = texture;

  rotateImage(image, isMainCar ? x : x - cameraPos.x, y, angle);
}

function drawInfo() {
  const y = cameraPos.y - info.height;

  ctx.drawImage(bg, 0, 0, canvas.width, canvas.height);
  ctx.drawImage(info, canvas.width / 2 - info.width / 2, y > 0 ? 0 : y);
}

function drawMaps() {
  maps.forEach((map, index) => {
    ctx.drawImage(
      map,
      canvas.width * index - cameraPos.x,
      0,
      canvas.width,
      canvas.height
    );
  });
}

function drawProgress() {
  const height = 14;
  const progress = canvas.width / mapsWidth;

  progressCar.width = 32;
  progressCar.height = 17;

  ctx.beginPath();
  ctx.fillStyle = "#FFEF00";
  ctx.fillRect(0, canvas.height - height / 2, canvas.width, height);
  ctx.closePath();

  ctx.beginPath();
  ctx.fillStyle = "#FA00FF";
  ctx.fillRect(0, canvas.height - height / 2, cameraPos.x * progress, height);
  ctx.closePath();

  ctx.beginPath();
  ctx.drawImage(
    progressCar,
    cameraPos.x * progress - progressCar.width / 2,
    canvas.height - progressCar.height / 2,
    progressCar.width,
    progressCar.height
  );
  ctx.closePath();
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if (steps[currentStep] === "instruction") {
    drawInfo();
    cameraPos.y += 5;
  }

  if (steps[currentStep] === "game") {
    drawMaps();

    textures.forEach((texture) => {
      drawTexture(texture);

      if (texture.isMainCar) {
        if (texture.y > canvas.height) {
          gameOver = true;
        }

        if (upPressed) {
          texture.y -= 7;
          texture.angle = -15;
        } else if (downPressed) {
          texture.y += 7;
          texture.angle = 15;
        } else if (rightPressed) {
          texture.x += 7;
        } else {
          texture.angle = 0;
          texture.x = texture.x > 100 ? (texture.x -= 2) : (texture.x = 100);
        }
      }
    });

    drawProgress();

    if (cameraPos.x < mapsWidth) {
      cameraPos.x += 5;
    }

    if (gameOver) {
      console.log(gameOver);
    }
  }

  requestAnimationFrame(draw);
}

group.onload = draw;
